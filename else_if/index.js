/**
 * if( điều kiện ){
 *  hành động
 * }else{
 *  hành động khi điều kiện trong if sai
 * }
 *
 */

var isLogin = true;

// if (isLogin) {
//   console.log("Chuyển hướng user về trang chủ");
// }
// if (!isLogin) {
//   console.log("Thông báo đăng nhập sai");
// }
if (isLogin) {
  console.log("Chuyển hướng user về trang chủ");
} else {
  console.log("Thông báo đăng nhập sai");
}
